# Build
FROM node:16 AS build

WORKDIR /app

COPY . .

RUN npm install --legacy-peer-deps && \
    npm run postinstall && \
    npm run build



# Prod
FROM build AS prod

WORKDIR /app

COPY --from=build /app/build ./build

EXPOSE 8080

CMD [ "npm", "run", "start" ]



# Dev
FROM node:16 AS dev

WORKDIR /app

VOLUME [ "/app" ]

EXPOSE 8080

CMD [ "sh", "-c", "npm install --legacy-peer-deps && npm run postinstall && npm run build && npm run serve" ]
