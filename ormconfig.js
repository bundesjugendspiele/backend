const fs = require('fs');

const databaseHost = process.env.MYSQL_HOST
	? process.env.MYSQL_HOST
	: 'localhost';

const databasePort = process.env.MYSQL_PORT
	? Number(process.env.MYSQL_PORT)
	: 3306;

const databaseUser =
	process.env.MYSQL_USER_FILE !== undefined
		? fs
				.readFileSync(process.env.MYSQL_USER_FILE, 'utf8')
				.replace(/\r|\n/g, '')
		: process.env.MYSQL_USER;
const databasePassword =
	process.env.MYSQL_PASSWORD_FILE !== undefined
		? fs
				.readFileSync(process.env.MYSQL_PASSWORD_FILE, 'utf8')
				.replace(/\r|\n/g, '')
		: process.env.MYSQL_PASSWORD;
const databaseName =
	process.env.MYSQL_DATABASE_FILE !== undefined
		? fs
				.readFileSync(process.env.MYSQL_DATABASE_FILE, 'utf8')
				.replace(/\r|\n/g, '')
		: process.env.MYSQL_DATABASE;

const dirname =
	__dirname + (process.env.NODE_ENV != 'dev' ? '/build' : '') + '/src';

module.exports = {
	type: 'mysql',
	host: databaseHost,
	port: databasePort,
	username: databaseUser,
	password: databasePassword,
	database: databaseName,
	entities: [dirname + '/entities/*Entity.{ts,js}'],
	seeds: [dirname + '/seeders/*Seeder.{ts,js}'],
	factories: [dirname + '/factories/*Factory.{ts,js}'],
	synchronize: true,
};
