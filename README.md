# Bundesjugendspiele - Backend

## Make a production build

```bash
# Install all required dependencies
npm install
# Build the application
npm run build
```

All production files should be available under the `dist` folder.

## Start the production server

```bash
# Install all required dependencies
npm install
# Build the application
npm run start
```

The production server should be available under [http://localhost:8080](http://localhost:8080)

## Start the development server

```bash
# Install all required dependencies
npm install
# Start the development server
npm run serve
```

The development server should be available under [http://localhost:8080](http://localhost:8080)

## About the application

### Authors

-   Daniél Kerkmann
-   Maik Mursall

### License

The code is licensed under the [EUPL 1.2 or higher](LICENSE).
