import {
	Body,
	Controller,
	Delete,
	Get,
	Path,
	Route,
	Post,
	Put,
	Query,
	SuccessResponse,
	Tags,
} from 'tsoa';

import Score from '../entities/scoreEntity';

import {
	ScoreService,
	ScorePagination,
	ScoreCreationParams,
	ScoreUpdateParams,
} from '../services/scoreService';

@Route('scores')
@Tags('Score')
export class ScoreController extends Controller {
	private service = new ScoreService();

	@Get()
	public async getScores(
		@Query() scoreOrder?: 'ASC' | 'DESC',
		@Query() stationId?: number
	): Promise<ScorePagination> {
		this.setStatus(200);
		if (stationId && scoreOrder) {
			return this.service.getAllForStationOrderByScore(
				stationId,
				scoreOrder
			);
		} else if (stationId) {
			return this.service.getAllForStation(stationId);
		} else if (scoreOrder) {
			return this.service.getAllOrderByScore(scoreOrder);
		}
		return this.service.getAll();
	}

	@Get('{stationId}/{studentId}')
	public async getScore(
		@Path() stationId: number,
		@Path() studentId: number
	): Promise<Score | null> {
		this.setStatus(200);
		return this.service.get(stationId, studentId);
	}

	@Post()
	@SuccessResponse('201', 'Created')
	public async createScore(
		@Body() requestBody: ScoreCreationParams
	): Promise<Score> {
		this.setStatus(201);
		return this.service.create(requestBody);
	}

	@Put('{stationId}/{studentId}')
	@SuccessResponse('200', 'Updated')
	public async updateScore(
		@Path() stationId: number,
		@Path() studentId: number,
		@Body() requestBody: ScoreUpdateParams
	): Promise<Score | null> {
		this.setStatus(200);
		return this.service.update(stationId, studentId, requestBody);
	}

	@Delete('{stationId}/{studentId}')
	@SuccessResponse('204', 'Deleted')
	public async deleteScore(
		@Path() stationId: number,
		@Path() studentId: number
	): Promise<void> {
		this.setStatus(204);
		return this.service.delete(stationId, studentId);
	}
}
