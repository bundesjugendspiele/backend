import {
	Body,
	Controller,
	Delete,
	Get,
	Path,
	Post,
	Put,
	Query,
	Route,
	SuccessResponse,
	Tags,
} from 'tsoa';

import Unit from '../entities/unitEntity';

import {
	UnitService,
	UnitPagination,
	UnitCreationParams,
	UnitUpdateParams,
} from '../services/unitService';

@Route('units')
@Tags('Unit')
export class UnitController extends Controller {
	private service = new UnitService();

	@Get()
	public async getUnits(@Query() name?: string): Promise<UnitPagination> {
		this.setStatus(200);
		if (name) {
			return this.service.getAllLikeName(name);
		}
		return this.service.getAll();
	}

	@Get('{unitId}')
	public async getUnit(@Path() unitId: number): Promise<Unit | null> {
		this.setStatus(200);
		return this.service.get(unitId);
	}

	@Post()
	@SuccessResponse('201', 'Created')
	public async createUnit(
		@Body() requestBody: UnitCreationParams
	): Promise<Unit> {
		this.setStatus(201);
		return this.service.create(requestBody);
	}

	@Put('{unitId}')
	@SuccessResponse('200', 'Updated')
	public async updateUnit(
		@Path() unitId: number,
		@Body() requestBody: UnitUpdateParams
	): Promise<Unit | null> {
		this.setStatus(200);
		return this.service.update(unitId, requestBody);
	}

	@Delete('{unitId}')
	@SuccessResponse('204', 'Deleted')
	public async deleteUnit(@Path() unitId: number): Promise<void> {
		this.setStatus(204);
		return this.service.delete(unitId);
	}
}
