import {
	Body,
	Controller,
	Delete,
	Get,
	Path,
	Post,
	Put,
	Query,
	Route,
	SuccessResponse,
	Tags,
} from 'tsoa';

import Station from '../entities/stationEntity';

import { ScorePagination } from '../services/scoreService';
import {
	StationService,
	StationPagination,
	StationCreationParams,
	StationUpdateParams,
} from '../services/stationService';

@Route('stations')
@Tags('Station')
export class StationController extends Controller {
	private service = new StationService();

	@Get()
	public async getStations(
		@Query() name?: string
	): Promise<StationPagination> {
		this.setStatus(200);
		if (name) {
			return this.service.getAllLikeName(name);
		}
		return this.service.getAll();
	}

	@Get('{stationId}')
	public async getStation(
		@Path() stationId: number
	): Promise<Station | null> {
		this.setStatus(200);
		return this.service.get(stationId);
	}

	@Get('{stationId}/scores')
	public async getStationScores(
		@Path() stationId: number,
		@Query() scoreOrder: 'ASC' | 'DESC' = 'DESC'
	): Promise<ScorePagination> {
		this.setStatus(200);
		return this.service.getByScoreOrder(stationId, scoreOrder);
	}

	@Post()
	@SuccessResponse('201', 'Created')
	public async createStation(
		@Body() requestBody: StationCreationParams
	): Promise<Station> {
		this.setStatus(201);
		return this.service.create(requestBody);
	}

	@Put('{stationId}')
	@SuccessResponse('200', 'Updated')
	public async updateStation(
		@Path() stationId: number,
		@Body() requestBody: StationUpdateParams
	): Promise<Station | null> {
		this.setStatus(200);
		return this.service.update(stationId, requestBody);
	}

	@Delete('{stationId}')
	@SuccessResponse('204', 'Deleted')
	public async deleteStation(@Path() stationId: number): Promise<void> {
		this.setStatus(204);
		return this.service.delete(stationId);
	}
}
