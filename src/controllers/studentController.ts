import {
	Body,
	Controller,
	Delete,
	Get,
	Path,
	Post,
	Put,
	Query,
	Route,
	SuccessResponse,
	Tags,
} from 'tsoa';

import Student from '../entities/studentEntity';

import {
	StudentService,
	StudentPagination,
	StudentCreationParams,
	StudentUpdateParams,
} from '../services/studentService';

@Route('students')
@Tags('Student')
export class StudentController extends Controller {
	private service = new StudentService();

	@Get()
	public async getStudents(
		@Query() name?: string
	): Promise<StudentPagination> {
		this.setStatus(200);
		const names = name?.split(' ');
		if (names?.length === 2) {
			return this.service.getAllLikeFirstnameAndLastname(
				names[0],
				names[1]
			);
		} else if (name) {
			return this.service.getAllLikeName(name);
		}
		return this.service.getAll();
	}

	@Get('{studentId}')
	public async getStudent(
		@Path() studentId: number
	): Promise<Student | null> {
		this.setStatus(200);
		return this.service.get(studentId);
	}

	@Post()
	@SuccessResponse('201', 'Created')
	public async createStudent(
		@Body() requestBody: StudentCreationParams
	): Promise<Student> {
		this.setStatus(201);
		return this.service.create(requestBody);
	}

	@Put('{studentId}')
	@SuccessResponse('200', 'Updated')
	public async updateStudent(
		@Path() studentId: number,
		@Body() requestBody: StudentUpdateParams
	): Promise<Student | null> {
		this.setStatus(200);
		return this.service.update(studentId, requestBody);
	}

	@Delete('{studentId}')
	@SuccessResponse('204', 'Deleted')
	public async deleteStudent(@Path() studentId: number): Promise<void> {
		this.setStatus(204);
		return this.service.delete(studentId);
	}
}
