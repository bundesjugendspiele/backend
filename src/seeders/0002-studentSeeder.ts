import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

import Student from '../entities/studentEntity';

export default class CreateStudents implements Seeder {
	public async run(factory: Factory, _connection: Connection): Promise<any> {
		await factory(Student)().createMany(100);
	}
}
