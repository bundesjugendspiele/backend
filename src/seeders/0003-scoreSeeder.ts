import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

import Score from '../entities/scoreEntity';

export default class CreateScores implements Seeder {
	public async run(factory: Factory, connection: Connection): Promise<any> {
		while ((await connection.getRepository(Score).count()) < 500) {
			await factory(Score)()
				.create()
				.catch(() => {});
		}
	}
}
