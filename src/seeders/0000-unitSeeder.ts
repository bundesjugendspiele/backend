import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

import Unit from '../entities/unitEntity';

export default class CreateUnits implements Seeder {
	public async run(_factory: Factory, connection: Connection): Promise<any> {
		await connection
			.createQueryBuilder()
			.insert()
			.into(Unit)
			.values([
				{ id: 1, nameSingular: 'Banane', namePlural: 'Bananen' },
				{ id: 2, nameSingular: 'Sekunde', namePlural: 'Sekunden' },
				{ id: 3, nameSingular: 'Gramm', namePlural: 'Gramm' },
				{ id: 4, nameSingular: 'Kilo', namePlural: 'Kilos' },
				{ id: 5, nameSingular: 'Meter', namePlural: 'Meters' },
			])
			.execute();
	}
}
