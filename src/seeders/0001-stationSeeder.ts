import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

import Station from '../entities/stationEntity';

export default class CreateStations implements Seeder {
	public async run(_factory: Factory, connection: Connection): Promise<any> {
		await connection
			.createQueryBuilder()
			.insert()
			.into(Station)
			.values([
				{ id: 1, name: 'Bananen sammeln', unitId: 1 },
				{ id: 2, name: 'Bananen essen', unitId: 1 },
				{ id: 3, name: 'Luft anhalten', unitId: 2 },
				{ id: 4, name: 'Wettlauf (1000 Meter)', unitId: 2 },
				{ id: 5, name: 'Hanteln stemmen (Liegen)', unitId: 4 },
				{ id: 6, name: 'Hanteln stemmen (Stand)', unitId: 4 },
				{ id: 7, name: 'Weitsprung', unitId: 5 },
				{ id: 8, name: 'Sprint (50 Meter)', unitId: 5 },
				{ id: 9, name: 'Sprint (100 Meter)', unitId: 5 },
				{ id: 10, name: 'Sprint (500 Meter)', unitId: 5 },
			])
			.execute();
	}
}
