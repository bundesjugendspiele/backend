import { getRepository, SelectQueryBuilder } from 'typeorm';
import { PaginationAwareObject } from 'typeorm-pagination/dist/helpers/pagination';

import Score from '../entities/scoreEntity';

export interface ScorePagination extends PaginationAwareObject {
	data: Array<Score> | Score;
}

export interface ScoreCreationParams {
	score: number;
	studentId: number;
	stationId: number;
}

export interface ScoreUpdateParams {
	score?: number;
}

export class ScoreService {
	private repository = getRepository(Score);

	private getScores(): SelectQueryBuilder<Score> {
		return this.repository
			.createQueryBuilder('score')
			.leftJoinAndSelect('score.student', 'student')
			.leftJoinAndSelect('score.station', 'station')
			.leftJoinAndSelect('station.unit', 'unit');
	}

	public async getAllOrderByScore(
		order: 'ASC' | 'DESC'
	): Promise<ScorePagination> {
		return this.getScores().orderBy('score.score', order).paginate();
	}

	public async getAllForStationOrderByScore(
		stationId: number,
		order: 'ASC' | 'DESC'
	): Promise<ScorePagination> {
		return this.getScores()
			.where('score.stationId = :stationId', { stationId })
			.orderBy('score.score', order)
			.paginate();
	}

	public async getAllForStation(stationId: number): Promise<ScorePagination> {
		return this.getScores()
			.where('score.stationId = :stationId', { stationId })
			.paginate();
	}

	public async getAll(): Promise<ScorePagination> {
		return this.getScores().paginate();
	}

	public async get(
		stationId: number,
		studentId: number
	): Promise<Score | null> {
		const score = await this.repository.findOne({ stationId, studentId });
		if (!score) return null;
		return score;
	}

	public async create(
		scoreCreationParams: ScoreCreationParams
	): Promise<Score> {
		const score = new Score();
		return this.repository.save({
			...score,
			...scoreCreationParams,
		});
	}

	public async update(
		stationId: number,
		studentId: number,
		scoreUpdateParams: ScoreUpdateParams
	): Promise<Score | null> {
		const score = await this.repository.findOne({ stationId, studentId });
		if (!score) return null;
		await this.repository.save({
			...score,
			...scoreUpdateParams,
		});
		return this.get(stationId, studentId);
	}

	public async delete(stationId: number, studentId: number): Promise<void> {
		await this.repository.delete({ stationId, studentId });
	}
}
