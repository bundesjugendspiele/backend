import { getRepository } from 'typeorm';
import { PaginationAwareObject } from 'typeorm-pagination/dist/helpers/pagination';

import Student from '../entities/studentEntity';

export interface StudentPagination extends PaginationAwareObject {
	data: Array<Student> | Student;
}

export interface StudentCreationParams {
	firstName: string;
	lastName: string;
}

export interface StudentUpdateParams {
	firstName?: string;
	lastName?: string;
}

export class StudentService {
	private repository = getRepository(Student);

	public async getAllLikeFirstnameAndLastname(
		firstName: string,
		lastName: string
	): Promise<StudentPagination> {
		return this.repository
			.createQueryBuilder('student')
			.where('student.firstName LIKE :firstName', {
				firstName: `%${firstName}%`,
			})
			.andWhere('student.lastName LIKE :lastName', {
				lastName: `%${lastName}%`,
			})
			.paginate();
	}

	public async getAllLikeName(name: string): Promise<StudentPagination> {
		return this.repository
			.createQueryBuilder('student')
			.where('student.firstName LIKE :firstName', {
				firstName: `%${name}%`,
			})
			.orWhere('student.lastName LIKE :lastName', {
				lastName: `%${name}%`,
			})
			.paginate();
	}

	public async getAll(): Promise<StudentPagination> {
		return this.repository.createQueryBuilder().paginate();
	}

	public async get(id: number): Promise<Student | null> {
		const student = await this.repository
			.createQueryBuilder('student')
			.whereInIds(id)
			.leftJoinAndSelect('student.scores', 'scores')
			.leftJoinAndSelect('scores.station', 'station')
			.leftJoinAndSelect('station.unit', 'unit')
			.getOne();

		if (!student) return null;
		return student;
	}

	public async create(
		studentCreationParams: StudentCreationParams
	): Promise<Student> {
		const student = new Student();
		return this.repository.save({
			...student,
			...studentCreationParams,
		});
	}

	public async update(
		id: number,
		studentUpdateParams: StudentUpdateParams
	): Promise<Student | null> {
		const student = await this.repository.findOne({ id });
		if (!student) return null;
		await this.repository.save({
			...student,
			...studentUpdateParams,
		});
		return this.get(id);
	}

	public async delete(id: number): Promise<void> {
		await this.repository.delete({ id });
	}
}
