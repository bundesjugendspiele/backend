import { getRepository } from 'typeorm';
import { PaginationAwareObject } from 'typeorm-pagination/dist/helpers/pagination';

import Score from '../entities/scoreEntity';
import Station from '../entities/stationEntity';

import { ScorePagination } from '../services/scoreService';

export interface StationPagination extends PaginationAwareObject {
	data: Array<Station> | Station;
}

export interface StationCreationParams {
	name: string;
	unitId: number;
}

export interface StationUpdateParams {
	name?: string;
	unitId?: number;
}

export class StationService {
	private repository = getRepository(Station);

	public async getAllLikeName(name: string): Promise<StationPagination> {
		return this.repository
			.createQueryBuilder('station')
			.leftJoinAndSelect('station.unit', 'unit')
			.where('station.name LIKE :name', {
				name: `%${name}%`,
			})
			.paginate();
	}

	public async getAll(): Promise<StationPagination> {
		return this.repository
			.createQueryBuilder('station')
			.leftJoinAndSelect('station.unit', 'unit')
			.paginate();
	}

	public async get(id: number): Promise<Station | null> {
		const station = await this.repository
			.createQueryBuilder('station')
			.leftJoinAndSelect('station.unit', 'unit')
			.leftJoinAndSelect('station.scores', 'scores')
			.leftJoinAndSelect('scores.student', 'student')
			.whereInIds(id)
			.getOne();
		if (!station) return null;
		return station;
	}

	public async getByScoreOrder(
		id: number,
		order: 'ASC' | 'DESC'
	): Promise<ScorePagination> {
		return getRepository(Score)
			.createQueryBuilder('score')
			.leftJoinAndSelect('score.student', 'student')
			.where('score.stationId = :stationId', { stationId: id })
			.orderBy('score.score', order)
			.paginate();
	}

	public async create(
		stationCreationParams: StationCreationParams
	): Promise<Station> {
		const station = new Station();
		return this.repository.save({
			...station,
			...stationCreationParams,
		});
	}

	public async update(
		id: number,
		stationUpdateParams: StationUpdateParams
	): Promise<Station | null> {
		const station = await this.repository.findOne({ id });
		if (!station) return null;
		await this.repository.save({
			...station,
			...stationUpdateParams,
		});
		return this.get(id);
	}

	public async delete(id: number): Promise<void> {
		await this.repository.delete({ id });
	}
}
