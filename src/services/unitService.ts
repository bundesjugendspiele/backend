import { getRepository } from 'typeorm';
import { PaginationAwareObject } from 'typeorm-pagination/dist/helpers/pagination';

import Unit from '../entities/unitEntity';

export interface UnitPagination extends PaginationAwareObject {
	data: Array<Unit> | Unit;
}

export interface UnitCreationParams {
	nameSingular: string;
	namePlural: string;
}

export interface UnitUpdateParams {
	nameSingular?: string;
	namePlural?: string;
}

export class UnitService {
	private repository = getRepository(Unit);

	public async getAllLikeName(name: string): Promise<UnitPagination> {
		return this.repository
			.createQueryBuilder('unit')
			.where('unit.nameSingular LIKE :nameSingular', {
				nameSingular: `%${name}%`,
			})
			.orWhere('unit.namePlural LIKE :namePlural', {
				namePlural: `%${name}%`,
			})
			.paginate();
	}

	public async getAll(): Promise<UnitPagination> {
		return this.repository.createQueryBuilder().paginate();
	}

	public async get(id: number): Promise<Unit | null> {
		const unit = await this.repository.findOne({ id: id });
		if (!unit) return null;
		return unit;
	}

	public async create(unitCreationParams: UnitCreationParams): Promise<Unit> {
		const unit = new Unit();
		return this.repository.save({
			...unit,
			...unitCreationParams,
		});
	}

	public async update(
		id: number,
		unitUpdateParams: UnitUpdateParams
	): Promise<Unit | null> {
		const unit = await this.repository.findOne({ id });
		if (!unit) return null;
		await this.repository.save({
			...unit,
			...unitUpdateParams,
		});
		return this.get(id);
	}

	public async delete(id: number): Promise<void> {
		await this.repository.delete({ id });
	}
}
