import {
	Column,
	CreateDateColumn,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import Station from './stationEntity';

@Entity()
export default class Unit {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column({
		length: 100,
	})
	nameSingular!: string;

	@Column({
		length: 100,
	})
	namePlural!: string;

	@OneToMany((_) => Station, (station) => station.unit, {
		onDelete: 'SET NULL',
	})
	stations!: Station[];

	@CreateDateColumn()
	createdAt!: Date;

	@UpdateDateColumn()
	updatedAt!: Date;
}
