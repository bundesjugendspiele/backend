import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	ManyToOne,
	UpdateDateColumn,
} from 'typeorm';

import Station from './stationEntity';
import Student from './studentEntity';

@Entity()
export default class Score {
	@Column('decimal', { precision: 9, scale: 3 })
	score!: number;

	@Column({ primary: true })
	studentId!: number;
	@ManyToOne((_) => Student, (student) => student.scores, {
		primary: true,
		onDelete: 'CASCADE',
	})
	@JoinColumn({ name: 'studentId', referencedColumnName: 'id' })
	student!: Student;

	@Column({ primary: true })
	stationId!: number;
	@ManyToOne((_) => Station, (station) => station.scores, {
		primary: true,
		onDelete: 'CASCADE',
	})
	@JoinColumn({ name: 'stationId', referencedColumnName: 'id' })
	station!: Station;

	@CreateDateColumn()
	createdAt!: Date;

	@UpdateDateColumn()
	updatedAt!: Date;
}
