import {
	Column,
	CreateDateColumn,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import Score from './scoreEntity';

@Entity()
export default class Student {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column({
		length: 100,
	})
	firstName!: string;

	@Column({
		length: 100,
	})
	lastName!: string;

	@OneToMany((_) => Score, (score) => score.student)
	scores!: Score[];

	@CreateDateColumn()
	createdAt!: Date;

	@UpdateDateColumn()
	updatedAt!: Date;
}
