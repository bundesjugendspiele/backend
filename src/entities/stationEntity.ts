import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import Score from './scoreEntity';
import Unit from './unitEntity';

@Entity()
export default class Station {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column({
		length: 100,
	})
	name!: string;

	@Column({ nullable: true })
	unitId?: number;
	@ManyToOne((_) => Unit, (unit) => unit.stations, {
		onDelete: 'SET NULL',
		nullable: true,
	})
	@JoinColumn({ name: 'unitId', referencedColumnName: 'id' })
	unit?: Unit;

	@OneToMany((_) => Score, (score) => score.station)
	scores!: Score[];

	@CreateDateColumn()
	createdAt!: Date;

	@UpdateDateColumn()
	updatedAt!: Date;
}
