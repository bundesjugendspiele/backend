import Faker from 'faker';
import { define } from 'typeorm-seeding';

import Score from '../entities/scoreEntity';

define(Score, (faker: typeof Faker) => {
	const score = new Score();
	score.studentId = faker.random.number(100 - 1) + 1;
	score.stationId = faker.random.number(10 - 1) + 1;
	score.score = faker.random.number(300000) * 0.001;
	return score;
});
