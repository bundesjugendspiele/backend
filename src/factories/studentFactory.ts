import Faker from 'faker';
import { define } from 'typeorm-seeding';

import Student from '../entities/studentEntity';

define(Student, (faker: typeof Faker) => {
	faker.locale = 'de';

	const student = new Student();
	student.firstName = faker.name.firstName();
	student.lastName = faker.name.lastName();
	return student;
});
