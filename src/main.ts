import express, { Response as ExResponse, Request as ExRequest } from 'express';
import { createConnection } from 'typeorm';
import bodyParser from 'body-parser';
import { pagination } from 'typeorm-pagination';
import swaggerUi from 'swagger-ui-express';
import { RegisterRoutes } from '../build/routes';

createConnection()
	.then(async (_connection) => {
		const PORT = process.env.PORT || 8080;

		const app = express();

		app.use(
			bodyParser.urlencoded({
				extended: true,
			})
		);
		app.use(bodyParser.json());

		app.use(pagination);

		app.use(
			'/swagger',
			swaggerUi.serve,
			async (_req: ExRequest, res: ExResponse) => {
				return res.send(
					swaggerUi.generateHTML(
						await import('../build/swagger.json')
					)
				);
			}
		);

		RegisterRoutes(app);

		app.listen(PORT, () => {
			console.log(`Server started at http://localhost:${PORT}`);
		});
	})
	.catch((error) => console.error(error));
